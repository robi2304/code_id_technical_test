﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ConsumeWebApi.Models
{
    public class CommentViewModel
    {
        public int PostId { get; set; }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Body { get; set; }

    }
}
