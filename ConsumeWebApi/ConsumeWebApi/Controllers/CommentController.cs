﻿using ConsumeWebApi.Models;
using ConsumeWebApi.Pagination;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ConsumeWebApi.Controllers
{
    public class CommentController : Controller
    {
        
        Uri baseAddress = new Uri("https://jsonplaceholder.typicode.com/comments");
        private readonly HttpClient _httpClient;

        public CommentController()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = baseAddress;
        }
        [HttpGet]
        public IActionResult Index(int? pageNumber, int? pageSize)
        {
            List<CommentViewModel> commentsList = new List<CommentViewModel>();
            HttpResponseMessage response = _httpClient.GetAsync(_httpClient.BaseAddress).Result;

            if(response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                commentsList = JsonConvert.DeserializeObject<List<CommentViewModel>>(data);
            }

            int defaultPageSize = 10;
            int currentPageNumber = pageNumber ?? 1;
            int currentPageSize = pageSize ?? defaultPageSize;

            var paginatedComments = PaginatedList<CommentViewModel>.Create(commentsList, currentPageNumber, currentPageSize);
            ViewBag.PageSize = currentPageSize;

            return View(paginatedComments);
        }
    }
}
